#!/usr/bin/env bash

set -e

user=${1-$USER}

npm install
npm run build
scp -r dist $user@login.toolforge.org:/data/project/wlh/dist
ssh $user@login.toolforge.org "
  set -ex
  become wlh bash -xc \"
    take dist &&
    set -e &&
    rm -rf public_html.bak &&
    mv public_html public_html.bak &&
    mv dist public_html &&
    webservice stop &&
    webservice start
  \"
"
