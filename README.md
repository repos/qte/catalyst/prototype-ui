## Installation and Local or Dockerized Development

Clone and switch to the project directory

```bash
git clone git@gitlab.wikimedia.org:repos/qte/catalyst/prototype-ui.git

cd prototype-ui
```

### Install dependencies and run locally

Requirements:
* NodeJS version 16+ (most recent LTS version is recommended)

```bash
git submodule update --init --recursive

npm install

npm run dev
```

### Install dependencies and run inside a Docker container

Requirements:
* Docker

```bash
./docker_install
```
