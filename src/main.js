import { createApp, ref } from 'vue';
import { createRouter, createWebHistory } from 'vue-router'
import { Icon } from '@iconify/vue'
import App from './App.vue';

const routes = [
  {
    name: 'Landing',
    path: '/',
    component: () => import('./components/Landing.vue'),
  },
  {
    name: 'Environments',
    path: '/environments',
    component: () => import('./components/Environments.vue'),
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

createApp( App )
	.use( router )
  .component('Icon', Icon)
	.mount( '#app' );
